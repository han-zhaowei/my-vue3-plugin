
import { createApp } from 'vue'
import App from './App.vue'
import {
    createVuesticEssential,
    VaButton,
    VaSelect,
    VaInput,
    VaDropdownPlugin,
  } from "vuestic-ui";
  const app = createApp(App)
  app.use(
    createVuesticEssential({
      components: { VaButton, VaSelect, VaInput },
      plugins: { VaDropdownPlugin },
      config: {
        /* ... */
      },
    })
  ).mount('#app')
