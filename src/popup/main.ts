import { createApp } from 'vue'
import app from './components/app.vue'
import '../assets/style/reset.css'
createApp(app).mount('#app')
